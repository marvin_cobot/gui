import { reactive } from 'vue'

export const store = reactive({
    plan: [],
    planning_level: 1,
    assembly_finished: false,
    empty_plan: false,
    robot_can_move: false,
    robot_motion_status: false,
    arm: null,
    pattern: null,
    vision_val_:false,
    robot_speed: 200,
    logs: [
        {
            time_stamp: new Date().toISOString(),
            message: " ============{ Logs }============",
            color: "white",
            name: 0,
        }
    ],
})